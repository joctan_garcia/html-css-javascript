window.addEventListener('load', function () {

    let button = document.createElement('button');
    button.innerText = 'Button';

    button.addEventListener('click', function () {
        console.log('click');
        let parrafo = document.createElement('p');
        parrafo.innerText = 'Nuevo párrafo creado';
        parrafo.classList.add('red');
        document.getElementById('id1').appendChild(parrafo);
    });

    document.getElementById('id1').appendChild(button);
})


/*
function hola() {
    let algo = 'hola';
    var algo2 = 'hola'

    console.log(algo);
    console.log(algo2);
}

console.log(algo);
console.log(algo2);

hola();
*/